FROM composer:1.8 AS vendors

RUN composer global req hirak/prestissimo

ADD composer.* ./

RUN composer install

FROM php:7.1

ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64  /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init
ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]

RUN mkdir /app
WORKDIR /app

COPY --from=vendors /app/vendor ./vendor
COPY src ./src
COPY tests ./tests

CMD ["/usr/local/bin/php", "tests/console", "server:run", "0.0.0.0:80"]
