<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Factory\Operation;

use Irstea\ApiMetadata\Exception\OperationNotFoundException;
use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Model\Identity\OperationIdentityInterface;
use Irstea\ApiMetadata\Model\Identity\ResourceIdentityInterface;
use Irstea\ApiMetadata\Model\OperationMetadata;

/**
 * Class NullOperationFactory.
 */
class NullOperationFactory implements OperationFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createOperation(OperationIdentityInterface $operationId, ContextInterface $context): OperationMetadata
    {
        throw new OperationNotFoundException('No operation metadata for ' . $operationId->getClass());
    }

    /**
     * {@inheritdoc}
     */
    public function enumerateOperations(ResourceIdentityInterface $resourceId): array
    {
        return [];
    }
}
