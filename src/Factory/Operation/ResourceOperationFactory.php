<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Factory\Operation;

use ApiPlatform\Core\Api\OperationMethodResolverInterface;
use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use ApiPlatform\Core\PathResolver\OperationPathResolverInterface;
use Irstea\ApiMetadata\Exception\InvalidArgumentException;
use Irstea\ApiMetadata\Exception\OperationNotFoundException;
use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Helper\PropertyInfoType;
use Irstea\ApiMetadata\Model\Identity\OperationIdentity;
use Irstea\ApiMetadata\Model\Identity\OperationIdentityInterface;
use Irstea\ApiMetadata\Model\Identity\ResourceIdentityInterface;
use Irstea\ApiMetadata\Model\OperationMetadata;
use Irstea\ApiMetadata\Model\TypeMetadata;

/**
 * Class ResourceOperationFactory.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects) @TODO Refactorer la classe
 */
class ResourceOperationFactory implements OperationFactoryInterface
{
    /** @var OperationFactoryInterface */
    private $next;

    /** @var OperationPathResolverInterface */
    private $pathResolver;

    /** @var OperationMethodResolverInterface */
    private $methodResolver;

    /** @var ResourceMetadataFactoryInterface */
    private $metadataFactory;

    /**
     * ResourceOperationFactory constructor.
     *
     * @param ResourceMetadataFactoryInterface $metadataFactory
     * @param OperationMethodResolverInterface $methodResolver
     * @param OperationPathResolverInterface   $pathResolver
     * @param OperationFactoryInterface        $next
     */
    public function __construct(
        ResourceMetadataFactoryInterface $metadataFactory,
        OperationMethodResolverInterface $methodResolver,
        OperationPathResolverInterface $pathResolver,
        OperationFactoryInterface $next
    ) {
        $this->next = $next;
        $this->pathResolver = $pathResolver;
        $this->methodResolver = $methodResolver;
        $this->metadataFactory = $metadataFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createOperation(OperationIdentityInterface $operationId, ContextInterface $context): OperationMetadata
    {
        $class = $operationId->getClass();
        $type = $operationId->getType();

        $resourceMetadata = $this->metadataFactory->create($class);

        switch ($operationId->getType()) {
             case OperationType::ITEM:
                 $ops = $resourceMetadata->getItemOperations();
                 break;

            case OperationType::COLLECTION:
                $ops = $resourceMetadata->getCollectionOperations();
                break;

            default:
                throw new \InvalidArgumentException("unhandled operation type: $type");
        }

        foreach ((array) $ops as $opName => $attributes) {
            if ($opName === $operationId->getName()) {
                return $this->extractMetadata($operationId, $resourceMetadata, $attributes, $context);
            }
        }

        throw new OperationNotFoundException("unknown ${type} operation: " . $operationId->getName());
    }

    /**
     * {@inheritdoc}
     */
    public function enumerateOperations(ResourceIdentityInterface $resourceId): array
    {
        $class = $resourceId->getClass();

        $operations = $this->next->enumerateOperations($resourceId);

        $resource = $this->metadataFactory->create($class);

        foreach ((array) $resource->getItemOperations() as $name => $attributes) {
            $operations[] = OperationIdentity::fromValues($resourceId->getClass(), $resourceId->getShortName(), OperationType::ITEM, $name);
        }

        foreach ((array) $resource->getCollectionOperations() as $name => $attributes) {
            $operations[] = OperationIdentity::fromValues($resourceId->getClass(), $resourceId->getShortName(), OperationType::COLLECTION, $name);
        }

        return $operations;
    }

    /**
     * @param OperationIdentityInterface $operationId
     * @param ResourceMetadata           $resource
     * @param array                      $attributes
     * @param ContextInterface           $context
     *
     * @return OperationMetadata
     */
    private function extractMetadata(OperationIdentityInterface $operationId, ResourceMetadata $resource, array $attributes, ContextInterface $context): OperationMetadata
    {
        $class = $operationId->getClass();
        $type = $operationId->getType();
        $name = $operationId->getName();

        $path = $this->pathResolver->resolveOperationPath($resource->getShortName() ?: $class, $attributes, $operationId->getType());
        $path = str_replace('.{_format}', '', $path);

        switch ($operationId->getType()) {
            case OperationType::ITEM:
                $method = $this->methodResolver->getItemOperationMethod($class, $name);
                $input = $resource->getItemOperationAttribute($name, 'input', $class, true);
                $output = $resource->getItemOperationAttribute($name, 'output', $class, true);
                break;

            case OperationType::COLLECTION:
                $method = $this->methodResolver->getCollectionOperationMethod($class, $name);
                $input = $resource->getCollectionOperationAttribute($name, 'input', $class, true);
                $output = $resource->getCollectionOperationAttribute($name, 'output', $class, true);
                break;

            default:
                throw new \InvalidArgumentException("unhandled operationt type: $type");
        }

        return new OperationMetadata(
            $operationId,
            $path,
            $method,
            $method !== 'GET' && $method !== 'HEAD' ? $this->findInOuts($input, $context) : null,
            $method !== 'HEAD' ? $this->findInOuts($output, $context) : null
        );
    }

    /**
     * @param array|string|false $definition
     * @param ContextInterface   $context
     *
     * @return TypeMetadata|null
     */
    private function findInOuts($definition, ContextInterface $context): ?TypeMetadata
    {
        if ($definition === false) {
            return null;
        }

        if (is_array($definition)) {
            if (!isset($definition['class'])) {
                return null;
            }
            $definition = $definition['class'];
        }

        if (is_string($definition)) {
            return $context->createType(PropertyInfoType::create($definition), $context);
        }

        throw new InvalidArgumentException('unknown input/output argument');
    }
}
