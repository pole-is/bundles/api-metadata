<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Factory\Property;

use ApiPlatform\Core\Exception\PropertyNotFoundException;
use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface;
use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Helper\PropertyInfoType;
use Irstea\ApiMetadata\Model\PropertyMetadata;
use Irstea\ApiMetadata\Model\ResourceLinkMetadata;
use Irstea\ApiMetadata\Model\ResourceMetadata;
use Irstea\ApiMetadata\Model\TypeMetadata;

/**
 * Class ResourcePropertyFactory.
 */
class ResourcePropertyFactory implements PropertyFactoryInterface
{
    /** @var PropertyNameCollectionFactoryInterface */
    private $namesFactory;

    /** @var PropertyMetadataFactoryInterface */
    private $metadataFactory;

    /** @var PropertyFactoryInterface */
    private $next;

    /**
     * ResourcePropertyFactory constructor.
     *
     * @param PropertyNameCollectionFactoryInterface $namesFactory
     * @param PropertyMetadataFactoryInterface       $metadataFactory
     * @param PropertyFactoryInterface|null          $next
     */
    public function __construct(
        PropertyNameCollectionFactoryInterface $namesFactory,
        PropertyMetadataFactoryInterface $metadataFactory,
        PropertyFactoryInterface $next = null
    ) {
        $this->namesFactory = $namesFactory;
        $this->metadataFactory = $metadataFactory;
        $this->next = $next ?: new NullPropertyFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function create(string $class, string $name, ContextInterface $context): PropertyMetadata
    {
        try {
            $property = $this->metadataFactory->create($class, $name);
        } catch (PropertyNotFoundException $ex) {
            return $this->next->create($class, $name, $context);
        }

        [$type, $typeMeta] = $this->getPropertyType($property, $context);

        return new PropertyMetadata(
            $name,
            $typeMeta,
            $type->isNullable() ?: false,
            $property->isReadable() ?: false,
            $property->isWritable() ?: false,
            $property->isInitializable() ?: false,
            $property->isIdentifier() ?: false
        );
    }

    /**
     * @param \ApiPlatform\Core\Metadata\Property\PropertyMetadata $property
     * @param ContextInterface                                     $context
     *
     * @return array
     */
    private function getPropertyType(\ApiPlatform\Core\Metadata\Property\PropertyMetadata $property, ContextInterface $context): array
    {
        $type = PropertyInfoType::notNull($property->getType());
        $typeMeta = $context->createType($type, $context);

        if (!$property->isReadableLink() && !$property->isWritableLink()) {
            $typeMeta = $typeMeta->fmap(static function (TypeMetadata $t) {
                if ($t instanceof ResourceMetadata) {
                    return new ResourceLinkMetadata($t);
                }

                return $t;
            });
        }

        return [$type, $typeMeta];
    }

    /**
     * {@inheritdoc}
     */
    public function enumerate(string $class, ContextInterface $context): array
    {
        $properties = $this->next->enumerate($class, $context);

        try {
            $names = $this->namesFactory->create($class);
        } catch (ResourceClassNotFoundException $ex) {
            return $properties;
        }

        foreach ($names as $name) {
            $properties[$name] = $this->create($class, $name, $context);
        }

        return $properties;
    }
}
