<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Factory\Type;

use Assert\Assertion;
use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Factory\Property\PropertyFactoryInterface;
use Irstea\ApiMetadata\Factory\ReferenceFactoryInterface;
use Irstea\ApiMetadata\Helper\PropertyInfoType;
use Irstea\ApiMetadata\Model\ObjectMetadata;
use Irstea\ApiMetadata\Model\TypeMetadata;

/**
 * Class ObjectTypeFactory.
 */
class ObjectTypeFactory extends AbstractClassTypeFactory
{
    /** @var PropertyFactoryInterface */
    private $propertyFactory;

    /**
     * ObjectTypeFactory constructor.
     *
     * @param PropertyFactoryInterface $propertyFactory
     * @param TypeFactoryInterface     $next
     */
    public function __construct(
        PropertyFactoryInterface $propertyFactory,
        TypeFactoryInterface $next
    ) {
        parent::__construct($next);
        $this->propertyFactory = $propertyFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function supportsClass(string $className, ContextInterface $context): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    protected function createClass(string $className, ContextInterface $context): TypeMetadata
    {
        if ($context instanceof ReferenceFactoryInterface) {
            return $context->getReference($className, function () use ($className, $context) {
                return $this->doCreateClass($className, $context);
            });
        }

        return  $this->doCreateClass($className, $context);
    }

    /**
     * @param string           $className
     * @param ContextInterface $context
     *
     * @throws \ReflectionException
     *
     * @return TypeMetadata
     */
    private function doCreateClass(string $className, ContextInterface $context): TypeMetadata
    {
        $reflection = new \ReflectionClass($className);

        if (($parent = $reflection->getParentClass()) !== false) {
            $parentMeta = $context->createType(PropertyInfoType::create($parent->getName()), $context);
        } else {
            $parentMeta = null;
        }

        Assertion::nullOrIsInstanceOf($parentMeta, ObjectMetadata::class);
        /* @var $parentMeta ObjectMetadata|null */

        return new ObjectMetadata(
            $className,
            $parentMeta,
            $this->propertyFactory->enumerate($className, $context)
        );
    }
}
