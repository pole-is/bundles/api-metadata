<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Factory\Type;

use Irstea\ApiMetadata\Exception\InvalidArgumentException;
use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Helper\PropertyInfoType;
use Irstea\ApiMetadata\Model\ArrayMetadata;
use Irstea\ApiMetadata\Model\MapMetadata;
use Irstea\ApiMetadata\Model\TypeMetadata;
use Symfony\Component\PropertyInfo\Type;

/**
 * Class CollectionTypeFactory.
 */
class CollectionTypeFactory extends TypeFactoryDecorator
{
    /**
     * {@inheritdoc}
     */
    public function createType(Type $type, ContextInterface $context): TypeMetadata
    {
        if (!$type->isCollection()) {
            return parent::createType($type, $context);
        }

        $valueType = PropertyInfoType::notNull($type->getCollectionValueType());
        $keyType = PropertyInfoType::notNull($type->getCollectionKeyType());

        switch ($keyType->getBuiltinType()) {
            case 'integer':
                return new ArrayMetadata(
                    $context->createType($valueType, $context)
                );

            case 'string':
                return new MapMetadata(
                    $context->createType($valueType, $context)
                );
        }

        throw new InvalidArgumentException('unsupported key type');
    }
}
