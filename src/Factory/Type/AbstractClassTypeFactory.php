<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Factory\Type;

use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Model\TypeMetadata;
use Symfony\Component\PropertyInfo\Type;

/**
 * Class AbstractClassTypeFactory.
 */
abstract class AbstractClassTypeFactory extends TypeFactoryDecorator
{
    /**
     * {@inheritdoc}
     */
    public function createType(Type $type, ContextInterface $context): TypeMetadata
    {
        $className = $type->getClassName();
        if (!$className || !$this->supportsClass($className, $context)) {
            return parent::createType($type, $context);
        }

        return $this->createClass($className, $context);
    }

    /**
     * @param string $className
     *
     * @return bool
     */
    abstract protected function supportsClass(string $className, ContextInterface $context): bool;

    /**
     * @param string $className
     *
     * @return TypeMetadata
     */
    abstract protected function createClass(string $className, ContextInterface $context): TypeMetadata;
}
