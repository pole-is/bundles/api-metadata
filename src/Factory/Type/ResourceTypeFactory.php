<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Factory\Type;

use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use Assert\Assertion;
use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Factory\Operation\OperationFactoryInterface;
use Irstea\ApiMetadata\Factory\Property\PropertyFactoryInterface;
use Irstea\ApiMetadata\Factory\ReferenceFactoryInterface;
use Irstea\ApiMetadata\Model\Identity\ResourceIdentity;
use Irstea\ApiMetadata\Model\ResourceMetadata;
use Irstea\ApiMetadata\Model\TypeMetadata;

/**
 * Class ResourceTypeFactory.
 */
class ResourceTypeFactory extends AbstractClassTypeFactory
{
    /** @var OperationFactoryInterface */
    private $operationFactory;

    /** @var PropertyFactoryInterface */
    private $propertyFactory;

    /** @var ResourceMetadataFactoryInterface */
    private $metadataFactory;

    /**
     * ResourceTypeFactory constructor.
     *
     * @param PropertyFactoryInterface         $propertyFactory
     * @param OperationFactoryInterface        $operationFactory
     * @param ResourceMetadataFactoryInterface $metadataFactory
     * @param TypeFactoryInterface             $next
     */
    public function __construct(
        PropertyFactoryInterface $propertyFactory,
        OperationFactoryInterface $operationFactory,
        ResourceMetadataFactoryInterface $metadataFactory,
        TypeFactoryInterface $next
    ) {
        parent::__construct($next);
        $this->operationFactory = $operationFactory;
        $this->propertyFactory = $propertyFactory;
        $this->metadataFactory = $metadataFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function supportsClass(string $className, ContextInterface $context): bool
    {
        try {
            $this->metadataFactory->create($className);

            return true;
        } catch (ResourceClassNotFoundException $ex) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function createClass(string $className, ContextInterface $context): TypeMetadata
    {
        if ($context instanceof ReferenceFactoryInterface) {
            return $context->getReference($className, function () use ($className, $context) {
                return $this->doCreateClass($className, $context);
            });
        }

        return $this->doCreateClass($className, $context);
    }

    /**
     * @param string           $className
     * @param ContextInterface $context
     *
     * @throws \ReflectionException
     *
     * @return TypeMetadata
     */
    private function doCreateClass(string $className, ContextInterface $context): TypeMetadata
    {
        $resource = $this->metadataFactory->create($className);
        $shortName = $resource->getShortName();
        Assertion::notNull($shortName);

        $resourceId = ResourceIdentity::fromValues($className, $shortName);

        return new ResourceMetadata(
            $resourceId,
            null, //$parentMeta,
            $this->propertyFactory->enumerate($className, $context),
            $this->operationFactory->enumerateOperations($resourceId)
        );
    }
}
