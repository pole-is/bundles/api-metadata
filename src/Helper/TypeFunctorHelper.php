<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Helper;

use Irstea\ApiMetadata\Model\TypeFunctor;

/**
 * Class TypeFunctorHelper.
 */
class TypeFunctorHelper
{
    /**
     * @param TypeFunctor[] $items
     * @param callable      $func
     *
     * @return TypeFunctor[]
     */
    public static function map(array $items, callable $func): array
    {
        return array_map(
            function (TypeFunctor $item) use ($func) {
                return self::null($item, $func);
            },
            $items
        );
    }

    /**
     * @param TypeFunctor|null $item
     * @param callable         $func
     *
     * @return TypeFunctor|null
     */
    public static function null(?TypeFunctor $item, callable $func): ?TypeFunctor
    {
        return $item ? $item->fmap($func) : null;
    }
}
