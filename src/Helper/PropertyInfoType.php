<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Helper;

use Symfony\Component\PropertyInfo\Type;

/**
 * Class PropertyInfoType.
 *
 * Helper pour générer facilement un Symfony\Component\PropertyInfo\Type à partir d'une chaîne.
 */
final class PropertyInfoType
{
    /**
     * @param string $def
     *
     * @return Type
     */
    public static function create(string $def): Type
    {
        if (\class_exists($def) || \interface_exists($def)) {
            return new Type('object', false, $def);
        }
        if (in_array($def, Type::$builtinTypes, true)) {
            return new Type($def, false, null, $def === 'array');
        }

        return AnyType::instance();
    }

    /**
     * @param Type|null $type
     *
     * @return Type
     */
    public static function notNull(?Type $type): Type
    {
        return $type ?: AnyType::instance();
    }

    /** Never instanciated */
    private function __construct()
    {
    }
}
