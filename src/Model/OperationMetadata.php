<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model;

use Irstea\ApiMetadata\Helper\TypeFunctorHelper;
use Irstea\ApiMetadata\Model\Identity\OperationIdentityInterface;

/**
 * Class OperationMetadata.
 */
final class OperationMetadata implements OperationIdentityInterface, TypeFunctor
{
    /** @var OperationIdentityInterface */
    private $id;

    /** @var string */
    private $path;

    /** @var string */
    private $method;

    /** @var TypeMetadata|null */
    private $input;

    /** @var TypeMetadata|null */
    private $output;

    /**
     * OperationMetadata constructor.
     *
     * @param OperationIdentityInterface $id
     * @param string                     $path
     * @param string                     $method
     * @param TypeMetadata|null          $input
     * @param TypeMetadata|null          $output
     */
    public function __construct(OperationIdentityInterface $id, string $path, string $method, ?TypeMetadata $input, ?TypeMetadata $output)
    {
        $this->id = $id;
        $this->path = $path;
        $this->method = $method;
        $this->input = $input;
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     */
    public function getShortName(): string
    {
        return $this->id->getShortName();
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return $this->id->getClass();
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return $this->id->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->id->getName();
    }

    /**
     * Get path.
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Get method.
     *
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get input.
     *
     * @return TypeMetadata|null
     */
    public function getInput(): ?TypeMetadata
    {
        return $this->input;
    }

    /** Get output.
     *
     * @return TypeMetadata|null
     */
    public function getOutput(): ?TypeMetadata
    {
        return $this->output;
    }

    /**
     * {@inheritdoc}
     */
    public function fmap(callable $func)
    {
        return new self(
            $this->id,
            $this->path,
            $this->method,
            TypeFunctorHelper::null($this->input, $func),
            TypeFunctorHelper::null($this->output, $func)
        );
    }
}
