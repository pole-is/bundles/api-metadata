<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model;

/**
 * Class PropertyAccess.
 */
class PropertyAccess implements PropertyAccessInterface
{
    /**
     * @var bool
     */
    private $readable;
    /**
     * @var bool
     */
    private $writable;
    /**
     * @var bool
     */
    private $initialisable;

    /**
     * PropertyAccess constructor.
     */
    public function __construct(
        bool $readable,
        bool $writable,
        bool $initialisable
    ) {
        $this->readable = $readable;
        $this->writable = $writable;
        $this->initialisable = $initialisable;
    }

    /**
     * Get readable.
     *
     * @return bool
     */
    public function isReadable(): bool
    {
        return $this->readable;
    }

    /**
     * Get writable.
     *
     * @return bool
     */
    public function isWritable(): bool
    {
        return $this->writable;
    }

    /**
     * Get initialisable.
     *
     * @return bool
     */
    public function isInitialisable(): bool
    {
        return $this->initialisable;
    }

    /**
     * @param PropertyAccessInterface $other
     *
     * @return bool
     */
    public function accept(PropertyAccessInterface $other): bool
    {
        return (!$this->readable || $other->isReadable())
                && (!$this - $this->writable || $other->isWritable())
            && (!$this - $this->initialisable || $other->isInitialisable());
    }
}
