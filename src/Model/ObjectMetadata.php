<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model;

use Assert\Assertion;
use Irstea\ApiMetadata\Helper\TypeFunctorHelper;

/**
 * Class ObjectMetadata.
 */
class ObjectMetadata implements SimpleTypeInterface
{
    /** @var string */
    private $class;

    /** @var ObjectMetadata|null */
    private $parent;

    /** @var array<string, PropertyMetadata> */
    private $properties;

    /**
     * ObjectMetadata constructor.
     *
     * @param string              $class
     * @param ObjectMetadata|null $parent
     * @param PropertyMetadata[]  $properties
     */
    public function __construct(string $class, ?ObjectMetadata $parent, array $properties)
    {
        Assertion::classExists($class);
        Assertion::allIsInstanceOf($properties, PropertyMetadata::class);

        $this->properties = $properties;
        $this->class = $class;
        $this->parent = $parent;
    }

    /**
     * Get class.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Get parent.
     *
     * @return ObjectMetadata|null
     */
    public function getParent(): ?ObjectMetadata
    {
        return $this->parent;
    }

    /**
     * Get properties.
     *
     * @return array<string, PropertyMetadata>
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseType(): string
    {
        return 'object';
    }

    /**
     * {@inheritdoc}
     */
    public function getTypeAttributes(): array
    {
        return ['additionalProperties' => false];
    }

    /**
     * {@inheritdoc}
     */
    public function fmap(callable $func)
    {
        return $func(new self(
            $this->class,
            TypeFunctorHelper::null($this->parent, $func),
            TypeFunctorHelper::map($this->properties, $func)
        ));
    }
}
