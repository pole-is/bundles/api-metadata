<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model;

/**
 * Class MapMetadata.
 */
final class MapMetadata implements SimpleTypeInterface
{
    /** @var TypeMetadata */
    private $value;

    /**
     * MapMetadata constructor.
     *
     * @param TypeMetadata $valueType
     */
    public function __construct(TypeMetadata $valueType)
    {
        $this->value = $valueType;
    }

    /**
     * Get value.
     *
     * @return TypeMetadata
     */
    public function getValue(): TypeMetadata
    {
        return $this->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseType(): string
    {
        return 'object';
    }

    /**
     * {@inheritdoc}
     */
    public function getTypeAttributes(): array
    {
        return ['additionalProperties' => $this->value];
    }

    /**
     * {@inheritdoc}
     */
    public function fmap(callable $func)
    {
        return $func(new self($this->value->fmap($func)));
    }
}
