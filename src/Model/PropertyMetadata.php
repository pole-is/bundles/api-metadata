<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model;

/**
 * Class PropertyMetadata.
 */
final class PropertyMetadata implements TypeFunctor, PropertyAccessInterface
{
    /** @var string */
    private $name;

    /** @var TypeMetadata */
    private $type;

    /** @var bool */
    private $nullable;

    /** @var bool */
    private $readable;

    /** @var bool */
    private $writable;

    /** @var bool */
    private $initialisable;

    /** @var bool */
    private $identifier;

    /**
     * PropertyMetadata constructor.
     *
     * @param string       $name
     * @param TypeMetadata $type
     * @param bool         $nullable
     * @param bool         $readable
     * @param bool         $writable
     * @param bool         $initialisable
     * @param bool         $identifier
     */
    public function __construct(string $name, TypeMetadata $type, bool $nullable, bool $readable, bool $writable, bool $initialisable, bool $identifier)
    {
        $this->name = $name;
        $this->type = $type;
        $this->nullable = $nullable;
        $this->readable = $readable;
        $this->writable = $writable;
        $this->initialisable = $initialisable;
        $this->identifier = $identifier;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get type.
     *
     * @return TypeMetadata
     */
    public function getType(): TypeMetadata
    {
        return $this->type;
    }

    /**
     * Get nullable.
     *
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return !$this->nullable;
    }

    /**
     * Get readable.
     *
     * @return bool
     */
    public function isReadable(): bool
    {
        return $this->readable;
    }

    /**
     * Get writable.
     *
     * @return bool
     */
    public function isWritable(): bool
    {
        return $this->writable;
    }

    /**
     * Get initialisable.
     *
     * @return bool
     */
    public function isInitialisable(): bool
    {
        return $this->initialisable;
    }

    /**
     * Get identifier.
     *
     * @return bool
     */
    public function isIdentifier(): bool
    {
        return $this->identifier;
    }

    /**
     * {@inheritdoc}
     */
    public function fmap(callable $func)
    {
        return new self(
            $this->name,
            $this->type->fmap($func),
            $this->nullable,
            $this->readable,
            $this->writable,
            $this->initialisable,
            $this->identifier
        );
    }
}
