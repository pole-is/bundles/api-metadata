<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model;

use Assert\Assertion;
use Irstea\ApiMetadata\Exception\ReferenceResolverException;

/**
 * Class Reference.
 */
class Reference implements TypeMetadata
{
    private const STATUS_NEW = 0;
    private const STATUS_RESOLVING = 1;
    private const STATUS_RESOLVED = 2;

    /** @var string */
    private $id;

    /**
     * @var TypeMetadata|null
     */
    private $target;

    /** @var callable */
    private $resolver;

    /** @var bool */
    private $status = self::STATUS_NEW;

    /**
     * Reference constructor.
     *
     * @param string   $id
     * @param callable $resolver
     */
    public function __construct(string $id, callable $resolver)
    {
        $this->id = $id;
        $this->resolver = $resolver;
    }

    /**
     * Get target.
     *
     * @return TypeMetadata
     */
    public function getTarget(): TypeMetadata
    {
        Assertion::choice($this->status, [self::STATUS_NEW, self::STATUS_RESOLVING, self::STATUS_RESOLVED]);

        switch ($this->status) {
            case self::STATUS_NEW:
                return $this->resolve();

            case self::STATUS_RESOLVING:
                return $this;

            case self::STATUS_RESOLVED:
                return $this->target;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function fmap(callable $func)
    {
        return new self(
            $this->id,
            function () use ($func) {
                return $this->getTarget()->fmap($func);
            }
        );
    }

    /**
     * @return TypeMetadata
     */
    private function resolve(): TypeMetadata
    {
        $this->status = self::STATUS_RESOLVING;

        try {
            $resolver = $this->resolver;
            Assertion::isCallable($resolver);

            $result = $resolver();
            Assertion::isInstanceOf($result, TypeMetadata::class);

            return $this->target = $result;
        } catch (\Exception $ex) {
            throw new ReferenceResolverException(
                sprintf('Resolving "%s", threw an exception: %s', $this->id, $ex->getMessage()),
                0,
                $ex
            );
        } finally {
            $this->status = self::STATUS_RESOLVED;
        }
    }
}
