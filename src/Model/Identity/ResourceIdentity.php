<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model\Identity;

use Assert\Assertion;

/**
 * Class ResourceIdentity.
 */
final class ResourceIdentity implements ResourceIdentityInterface
{
    /**
     * @var string
     */
    private $shortName;
    /**
     * @var string
     */
    private $class;

    /**
     * ResourceIdentifiers constructor.
     *
     * @param string $class
     * @param string $shortName
     */
    private function __construct(string $class, string $shortName)
    {
        Assertion::classExists($class);

        $this->shortName = $shortName;
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Get shortName.
     *
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->shortName;
    }

    /**
     * @param string $class
     * @param string $shortName
     *
     * @return self
     */
    public static function fromValues(string $class, string $shortName): self
    {
        return new self($class, $shortName);
    }

    /**
     * @param ResourceIdentityInterface $resource
     *
     * @return self
     */
    public static function fromResource(ResourceIdentityInterface $resource): self
    {
        if ($resource instanceof self) {
            return $resource;
        }

        return new self($resource->getClass(), $resource->getShortName());
    }
}
