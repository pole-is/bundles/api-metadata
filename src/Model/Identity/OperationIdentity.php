<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Model\Identity;

use ApiPlatform\Core\Api\OperationType;
use Assert\Assertion;

/**
 * Class OperationID.
 */
final class OperationIdentity implements OperationIdentityInterface
{
    /** @var ResourceIdentityInterface */
    private $resourceId;

    /** @var string */
    private $type;

    /** @var string */
    private $name;

    /**
     * OperationIdentifiers constructor.
     *
     * @param ResourceIdentityInterface $resourceId
     * @param string                    $type
     * @param string                    $name
     */
    public function __construct(ResourceIdentityInterface $resourceId, string $type, string $name)
    {
        Assertion::choice($type, OperationType::TYPES);

        $this->resourceId = $resourceId;
        $this->type = $type;
        $this->name = $name;
    }

    /**
     * Get shortName.
     *
     * @return string
     */
    public function getClass(): string
    {
        return $this->resourceId->getClass();
    }

    /**
     * Get shortName.
     *
     * @return string
     */
    public function getShortName(): string
    {
        return $this->resourceId->getShortName();
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s::%s_%s', $this->resourceId->getShortName(), $this->name, $this->type);
    }

    /**
     * @param string $class
     * @param string $shortName
     * @param string $type
     * @param string $name
     *
     * @return self
     */
    public static function fromValues(string $class, string $shortName, string $type, string $name): self
    {
        return new self(ResourceIdentity::fromValues($class, $shortName), $type, $name);
    }

    /**
     * @param OperationIdentityInterface $operation
     *
     * @return self
     */
    public static function fromOperation(OperationIdentityInterface $operation): self
    {
        if ($operation instanceof self) {
            return $operation;
        }

        return new self(ResourceIdentity::fromValues($operation->getClass(), $operation->getShortName()), $operation->getType(), $operation->getName());
    }
}
