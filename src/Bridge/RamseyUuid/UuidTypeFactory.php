<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Bridge\RamseyUuid;

use Irstea\ApiMetadata\Factory\ContextInterface;
use Irstea\ApiMetadata\Factory\Type\AbstractClassTypeFactory;
use Irstea\ApiMetadata\Model\FormattedStringMetadata;
use Irstea\ApiMetadata\Model\TypeMetadata;

/**
 * Class UuidTypeFactory.
 */
class UuidTypeFactory extends AbstractClassTypeFactory
{
    /**
     * {@inheritdoc}
     */
    protected function supportsClass(string $className, ContextInterface $context): bool
    {
        /* @noinspection ClassConstantCanBeUsedInspection */
        return is_a($className, 'Ramsey\Uuid\UuidInterface', true);
    }

    /**
     * {@inheritdoc}
     */
    protected function createClass(string $className, ContextInterface $context): TypeMetadata
    {
        return FormattedStringMetadata::create('uuid');
    }
}
