<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Bridge\Symfony\Bundle\URI;

use Assert\Assertion;
use Irstea\ApiMetadata\Model\Identity\OperationIdentityInterface;
use Irstea\ApiMetadata\URI\URIGeneratorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class OperationMetadataURIGenerator.
 */
class OperationMetadataURIGenerator implements URIGeneratorInterface
{
    /** @var UrlGeneratorInterface */
    private $urlGenerator;

    /**
     * ObjectMetadataURIGenerator constructor.
     */
    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function generateURI($operation): string
    {
        Assertion::isInstanceOf($operation, OperationIdentityInterface::class);
        /* @var OperationIdentityInterface $operation */

        return $this->urlGenerator->generate(
            'api_metadata_operation_get',
            [
                    'shortName' => $operation->getShortName(),
                    'type'      => $operation->getType(),
                    'name'      => $operation->getName(),
                ],
            UrlGeneratorInterface::ABSOLUTE_URL
            ) . '#';
    }

    /**
     * {@inheritdoc}
     */
    public function supports($operation): bool
    {
        return $operation instanceof OperationIdentityInterface;
    }
}
