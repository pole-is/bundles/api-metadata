<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Bridge\Symfony\Bundle\Controller;

use Irstea\ApiMetadata\Factory\Type\TypeFactoryInterface;
use Irstea\ApiMetadata\Service\ResourceMapInterface;
use Irstea\ApiMetadata\URI\URIGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AbstractController.
 */
abstract class AbstractController
{
    /**
     * @var ResourceMapInterface
     */
    protected $resourceMap;

    /**
     * @var TypeFactoryInterface
     */
    protected $typeFactory;

    /** @var SerializerInterface */
    protected $serializer;

    /** @var URIGeneratorInterface */
    protected $uriGenerator;

    /**
     * ResourceListAction constructor.
     *
     * @param ResourceMapInterface  $resourceMap
     * @param TypeFactoryInterface  $typeFactory
     * @param SerializerInterface   $serializer
     * @param URIGeneratorInterface $uriGenerator
     */
    public function __construct(
        ResourceMapInterface $resourceMap,
        TypeFactoryInterface $typeFactory,
        SerializerInterface $serializer,
        URIGeneratorInterface $uriGenerator
    ) {
        $this->resourceMap = $resourceMap;
        $this->typeFactory = $typeFactory;
        $this->serializer = $serializer;
        $this->uriGenerator = $uriGenerator;
    }
}
