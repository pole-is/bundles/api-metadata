<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Bridge\Symfony\Bundle\Controller;

use Irstea\ApiMetadata\Bridge\Symfony\Serializer\ObjectMetadataNormalizer;
use Irstea\ApiMetadata\Factory\Context;
use Irstea\ApiMetadata\Factory\Operation\OperationFactoryInterface;
use Irstea\ApiMetadata\Factory\Type\TypeFactoryInterface;
use Irstea\ApiMetadata\Model\Identity\OperationIdentity;
use Irstea\ApiMetadata\Service\ResourceMapInterface;
use Irstea\ApiMetadata\URI\URIGeneratorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class OperationController.
 */
class OperationController extends AbstractController
{
    /**
     * @var OperationFactoryInterface
     */
    private $operationFactory;

    /**
     * OperationListAction constructor.
     *
     * @param ResourceMapInterface      $resourceMap
     * @param TypeFactoryInterface      $typeFactory
     * @param SerializerInterface       $serializer
     * @param URIGeneratorInterface     $uriGenerator
     * @param OperationFactoryInterface $operationFactory
     */
    public function __construct(
        ResourceMapInterface $resourceMap,
        TypeFactoryInterface $typeFactory,
        SerializerInterface $serializer,
        URIGeneratorInterface $uriGenerator,
        OperationFactoryInterface $operationFactory
    ) {
        parent::__construct($resourceMap, $typeFactory, $serializer, $uriGenerator);
        $this->operationFactory = $operationFactory;
    }

    /**
     * @param string $shortName
     * @param string $type
     * @param string $name
     *
     * @return Response
     */
    public function item(string $shortName, string $type, string $name): Response
    {
        $resourceId = $this->resourceMap->getByShortName($shortName);

        $operationId = OperationIdentity::fromValues(
            $resourceId->getClass(),
            $resourceId->getShortName(),
            $type,
            $name
        );

        $ctx = new Context($this->typeFactory);
        $metadata = $this->operationFactory->createOperation($operationId, $ctx);

        $json = $this->serializer->serialize($metadata, 'json', [ObjectMetadataNormalizer::BASE_URI_CTX_KEY => $this->uriGenerator->generateURI($operationId)]);

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @param string $shortName
     *
     * @return Response
     */
    public function collection(Request $request, string $shortName): Response
    {
        $resourceId = $this->resourceMap->getByShortName($shortName);
        $operations = [];

        foreach ($this->operationFactory->enumerateOperations($resourceId) as $operationId) {
            $operations[] = ['$ref' => $this->uriGenerator->generateURI($operationId)];
        }

        return JsonResponse::create([
            '$id'        => $request->getUri(),
            'operations' => $operations,
        ]);
    }
}
