<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Bridge\Symfony\Bundle\Controller;

use Irstea\ApiMetadata\Bridge\Symfony\Serializer\ObjectMetadataNormalizer;
use Irstea\ApiMetadata\Factory\Context;
use Irstea\ApiMetadata\Helper\PropertyInfoType;
use Irstea\ApiMetadata\Model\Identity\ResourceIdentityInterface;
use Irstea\ApiMetadata\Model\Reference;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ResourceController.
 */
class ResourceController extends AbstractController
{
    /**
     * @param string $shortName
     *
     * @return Response
     */
    public function item(string $shortName): Response
    {
        $resourceId = $this->resourceMap->getByShortName($shortName);

        $ctx = new Context($this->typeFactory);

        $type = PropertyInfoType::create($resourceId->getClass());

        $metadata = $ctx->createType($type, $ctx);
        if ($metadata instanceof Reference) {
            $metadata = $metadata->getTarget();
        }

        $json = $this->serializer->serialize($metadata, 'json', [ObjectMetadataNormalizer::BASE_URI_CTX_KEY => $this->uriGenerator->generateURI($resourceId)]);

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function collection(Request $request): Response
    {
        $resources = [];
        /** @var ResourceIdentityInterface $resourceId */
        foreach ($this->resourceMap as $resourceId) {
            $resources[$resourceId->getShortName()] = ['$ref' => $this->uriGenerator->generateURI($resourceId)];
        }

        return JsonResponse::create([
            '$id'       => $request->getUri(),
            'resources' => $resources,
        ]);
    }
}
