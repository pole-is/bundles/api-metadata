<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Bridge\Symfony\Serializer;

use Assert\Assertion;
use Irstea\ApiMetadata\Model\SimpleTypeInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class SimpleTypeNormalizer.
 */
class SimpleTypeNormalizer implements NormalizerAwareInterface, NormalizerInterface
{
    use NormalizerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof SimpleTypeInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        Assertion::isInstanceOf($object, SimpleTypeInterface::class);
        /** @var SimpleTypeInterface $object */
        $attrs = $object->getTypeAttributes();

        $normalizedAttrs = $this->normalizer->normalize($attrs, $format, $context);
        Assertion::isArray($normalizedAttrs);

        return (object) (['type' => $object->getBaseType()] + $normalizedAttrs);
    }
}
