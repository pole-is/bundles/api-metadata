<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Bridge\Symfony\Serializer;

use Assert\Assertion;
use Irstea\ApiMetadata\JSON\ClassMapping;
use Irstea\ApiMetadata\JSON\ClassMappingInterface;
use Irstea\ApiMetadata\JSON\Document;
use Irstea\ApiMetadata\Model\ObjectMetadata;
use Irstea\ApiMetadata\Model\PropertyMetadata;
use Irstea\ApiMetadata\Model\ResourceMetadata;
use Irstea\ApiMetadata\URI\URIGeneratorInterface;
use Irstea\ApiMetadata\URI\Util;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class ObjectMetadataNormalizer.
 */
class ObjectMetadataNormalizer implements NormalizerAwareInterface, NormalizerInterface
{
    public const BASE_URI_CTX_KEY = '$id';

    private const URI_GENERATOR_CTX_KEY = __CLASS__ . '->uriGenerator';

    private const CLASS_MAPPING_KEY = __CLASS__ . '->classMapping';

    use NormalizerAwareTrait;

    /**
     * @var URIGeneratorInterface
     */
    private $uriGenerator;

    /**
     * ObjectMetadataNormalizer constructor.
     *
     * @param URIGeneratorInterface $uriGenerator
     */
    public function __construct(URIGeneratorInterface $uriGenerator)
    {
        $this->uriGenerator = $uriGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ObjectMetadata;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = [])
    {
        /* @var ObjectMetadata $object */
        Assertion::isInstanceOf($object, ObjectMetadata::class);

        if ($object instanceof ResourceMetadata && !isset($context[self::CLASS_MAPPING_KEY])) {
            return (object) $this->normalizeRoot($object, $format, $context);
        }

        return (object) $this->normalizeNonRoot($object, $format, $context);
    }

    /**
     * @param ResourceMetadata $root
     * @param string|null      $format
     * @param array            $context
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     *
     * @return mixed
     */
    private function normalizeRoot(ResourceMetadata $root, $format, array $context)
    {
        $rootURI = Util::getDocumentURI($context[self::BASE_URI_CTX_KEY] ?? $this->generateURI($root, $context));

        $document = new Document($rootURI);
        $mapping = new ClassMapping($document, $rootURI, $this->uriGenerator);

        $context[self::CLASS_MAPPING_KEY] = $mapping;
        $context[self::BASE_URI_CTX_KEY] = $rootURI;

        $mapping->resolve(
            $root,
            function () use ($root, $format, $context) {
                return $this->doNormalize($root, $format, $context);
            }
        );

        $operations = [];
        foreach ($root->getOperations() as $operation) {
            $uri = $this->generateURI($operation, $context);
            $operations[] = ['$ref' => Util::getRelativeURI($rootURI, $uri)];
        }
        $document['#/operations'] = $operations;

        return $document->toArray();
    }

    /**
     * @param mixed $resource
     * @param array $context
     * @param bool  $relative
     *
     * @return string
     */
    private function generateURI($resource, array $context): string
    {
        $uriGenerator = $context[self::URI_GENERATOR_CTX_KEY] ?? $this->uriGenerator;

        return $uriGenerator->generateURI($resource);
    }

    /**
     * @param ObjectMetadata $object
     * @param string|null    $format
     * @param array          $context
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     *
     * @return array
     */
    private function normalizeNonRoot(ObjectMetadata $object, $format, array $context)
    {
        $mapping = $context[self::CLASS_MAPPING_KEY];
        Assertion::isInstanceOf($mapping, ClassMappingInterface::class);
        /** @var ClassMappingInterface $mapping */
        $ref = $mapping->resolve(
            $object,
            function () use ($object, $format, $context) {
                return $this->doNormalize($object, $format, $context);
            }
        );

        return ['$ref' => Util::getRelativeURI($context[self::BASE_URI_CTX_KEY], $ref)];
    }

    /**
     * @param ObjectMetadata $object
     * @param string|null    $format
     * @param array          $context
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     *
     * @return array
     */
    private function doNormalize(ObjectMetadata $object, $format, array $context): array
    {
        $attrs = $object->getTypeAttributes();

        $normalizedAttrs = $this->normalizer->normalize($attrs, $format, $context);
        Assertion::isArray($normalizedAttrs);

        $required = [];
        $properties = [];

        /** @var PropertyMetadata $property */
        foreach ($object->getProperties() as $property) {
            $properties[$property->getName()] = $this->normalizer->normalize($property, $format, $context);
            if ($property->isRequired()) {
                $required[] = $property->getName();
            }
        }

        return [
                'type'       => $object->getBaseType(),
                'required'   => $required,
                'properties' => $properties ?: (object) [],
            ] + $normalizedAttrs;
    }
}
