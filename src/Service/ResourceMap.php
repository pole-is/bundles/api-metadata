<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Service;

use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface;
use ArrayIterator;
use Assert\Assertion;
use Irstea\ApiMetadata\Exception\ResourceNotFoundException;
use Irstea\ApiMetadata\Model\Identity\ResourceIdentity;
use Irstea\ApiMetadata\Model\Identity\ResourceIdentityInterface;
use Iterator;

/**
 * Class ResourceMap.
 */
class ResourceMap implements ResourceMapInterface
{
    /** @var array<string, ResourceIdentityInterface>|null */
    private $resourceMap = null;

    /** @var array<string, ResourceIdentityInterface>|null */
    private $reverseMap = null;

    /** @var ResourceMetadataFactoryInterface */
    private $metadataFactory;

    /** @var ResourceNameCollectionFactoryInterface */
    private $nameFactory;

    /**
     * ResourceMap constructor.
     *
     * @param ResourceNameCollectionFactoryInterface $nameFactory
     * @param ResourceMetadataFactoryInterface       $metadataFactory
     */
    public function __construct(
        ResourceNameCollectionFactoryInterface $nameFactory,
        ResourceMetadataFactoryInterface $metadataFactory
    ) {
        $this->nameFactory = $nameFactory;
        $this->metadataFactory = $metadataFactory;
    }

    /**
     * @return Iterator<string, ResourceIdentityInterface>
     */
    public function getIterator(): Iterator
    {
        $this->init();
        Assertion::notNull($this->resourceMap);

        /** @var Iterator<string, ResourceIdentityInterface> $iter */
        $iter = new ArrayIterator($this->resourceMap);

        return $iter;
    }

    /**
     * {@inheritdoc}
     */
    public function isResourceClass(string $className): bool
    {
        $this->init();

        return isset($this->resourceMap[$className]);
    }

    /**
     * {@inheritdoc}
     */
    public function isResourceName(string $shortName): bool
    {
        $this->init();

        return isset($this->reverseMap[$shortName]);
    }

    /**
     * {@inheritdoc}
     */
    public function getByClassName(string $className): ResourceIdentityInterface
    {
        $this->init();

        $resourceId = $this->resourceMap[$className] ?? null;
        if ($resourceId === null) {
            throw new ResourceNotFoundException("No ressource found for class $className");
        }

        return $resourceId;
    }

    /**
     * {@inheritdoc}
     */
    public function getByShortName(string $shortName): ResourceIdentityInterface
    {
        $this->init();

        $resourceId = $this->reverseMap[$shortName] ?? null;
        if ($resourceId === null) {
            throw new ResourceNotFoundException("No ressource found for name $shortName");
        }

        return $resourceId;
    }

    /**
     * Initialise les maps de correspondance shortName <=> className.
     *
     * @throws ResourceClassNotFoundException
     */
    private function init(): void
    {
        if ($this->resourceMap !== null) {
            return;
        }
        $this->resourceMap = [];
        $this->reverseMap = [];

        foreach ($this->nameFactory->create() as $className) {
            $metadata = $this->metadataFactory->create($className);

            $shortName = $metadata->getShortName();
            Assertion::notNull($shortName);

            $this->resourceMap[$className] = $this->reverseMap[$shortName] = ResourceIdentity::fromValues($className, $shortName);
        }
    }
}
