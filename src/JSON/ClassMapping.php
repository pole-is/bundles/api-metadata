<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\JSON;

use Irstea\ApiMetadata\URI\URIGeneratorInterface;
use Irstea\ApiMetadata\URI\Util;

/**
 * Class ClassMapping.
 */
class ClassMapping implements ClassMappingInterface
{
    /**
     * @var Document
     */
    private $document;

    /**
     * @var string
     */
    private $base;

    /**
     * @var array<string, string>
     */
    private $references = [];

    /**
     * @var URIGeneratorInterface
     */
    private $uriGenerator;

    /**
     * ClassMapping constructor.
     *
     * @param Document              $document
     * @param string                $base
     * @param URIGeneratorInterface $uriGenerator
     */
    public function __construct(Document $document, string $base, URIGeneratorInterface $uriGenerator)
    {
        $this->document = $document;
        $this->base = $base;
        $this->uriGenerator = $uriGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function resolve($resource, callable $resolver): string
    {
        $uri = $this->uriGenerator->generateURI($resource);

        if (isset($this->references[$uri])) {
            return $this->references[$uri];
        }
        $this->references[$uri] = $uri;

        $ref = Util::getFragmentInDocument($this->base, $uri);

        if ($ref !== null) {
            $this->document[$ref] = $resolver();
        }

        return $uri;
    }
}
