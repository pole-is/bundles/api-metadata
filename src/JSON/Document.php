<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\JSON;

use Irstea\ApiMetadata\URI\Util;

/**
 * Class Document.
 */
class Document implements \ArrayAccess
{
    /** @var string */
    private $uri;

    /**
     * @var array<string, mixed>
     */
    private $data;

    /**
     * Document constructor.
     *
     * @param string $uri
     */
    public function __construct(string $uri)
    {
        $this->uri = Util::getDocumentURI($uri);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset): bool
    {
        $path = Util::getFragmentInDocument($this->uri, $offset);

        return $path && \array_key_exists($path, $this->data);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        $path = Util::getFragmentInDocument($this->uri, $offset);

        return $path !== null ? $this->data[$path] ?? null : null;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value): void
    {
        $path = Util::getFragmentInDocument($this->uri, $offset);

        if ($path !== null) {
            $this->data[$path] = $value;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset): void
    {
        $path = Util::getFragmentInDocument($this->uri, $offset);

        if ($path !== null) {
            unset($this->data[$path]);
        }
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $parts = [['$id' => $this->uri]];

        $paths = array_keys($this->data);
        usort($paths, [self::class, 'comparePaths']);

        foreach ($paths as $path) {
            if ($path === '#') {
                $parts[] = $this->data[$path];
                continue;
            }

            $root = [];
            $steps = explode('/', $path);
            array_shift($steps);
            $current = &$root;
            foreach ($steps as $next) {
                if ($next === '') {
                    $next = '0';
                }
                if (!isset($current[$next])) {
                    $current[$next] = [];
                }
                $current = &$current[$next];
            }
            $current = $this->data[$path];
            $parts[] = $root;
        }

        return \array_replace_recursive(...$parts);
    }

    /**
     * @param string $a
     * @param string $b
     *
     * @return bool
     */
    public static function comparePaths(string $left, string $right): bool
    {
        $leftParts = explode('/', $left);
        $rightParts = explode('/', $right);

        return $leftParts > $rightParts;
    }
}
