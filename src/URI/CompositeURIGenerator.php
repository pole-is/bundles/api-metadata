<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\URI;

use Assert\Assertion;

/**
 * Class CompositeURIGenerator.
 */
class CompositeURIGenerator implements URIGeneratorInterface
{
    /**
     * @var iterable<URIGeneratorInterface>
     */
    private $generators;

    /**
     * CompositeURIGenerator constructor.
     *
     * @param iterable<URIGeneratorInterface> $generators
     */
    public function __construct(iterable $generators)
    {
        Assertion::allIsInstanceOf($generators, URIGeneratorInterface::class);

        $this->generators = $generators;
    }

    /**
     * {@inheritdoc}
     */
    public function generateURI($resource): string
    {
        foreach ($this->generators as $generator) {
            if ($generator->supports($resource)) {
                return $generator->generateURI($resource);
            }
        }
        throw new \RuntimeException("cannot generate URI for resource ${resource}]");
    }

    /**
     * {@inheritdoc}
     */
    public function supports($resource): bool
    {
        foreach ($this->generators as $generator) {
            if ($generator->supports($resource)) {
                return true;
            }
        }

        return false;
    }
}
