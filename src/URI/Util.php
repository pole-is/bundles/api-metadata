<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\URI;

use function Sabre\Uri\build;
use function Sabre\Uri\parse;

/**
 * Class Util.
 */
final class Util
{
    /**
     * @param string $uri
     *
     * @return [string, string]
     */
    private static function split(string $uri): array
    {
        $parts = parse($uri);
        $fragment = $parts['fragment'] ?: '';
        unset($parts['fragment']);

        return [build($parts), $fragment];
    }

    /**
     * @param string $uri
     *
     * @return string
     */
    public static function getDocumentURI(string $uri): string
    {
        [$document, ] = self::split($uri);

        return $document;
    }

    /**
     * @param string $document
     * @param string $target
     *
     * @return string|null
     */
    public static function getFragmentInDocument(string $document, string $target): ?string
    {
        [$targetDocument, $targetFragment] = self::split($target);

        if (!$targetDocument || $targetDocument === self::getDocumentURI($document)) {
            return '#' . $targetFragment;
        }

        return null;
    }

    /**
     * @param string $base
     * @param string $target
     *
     * @return string
     */
    public static function getRelativeURI(string $base, string $target): string
    {
        [$targetDocument, $targetFragment] = self::split($target);
        if (!$targetDocument) {
            return '#' . $targetFragment;
        }

        [$baseDocument, ] = self::split($base);
        if ($targetDocument === $baseDocument) {
            $targetDocument = '';
        } else {
            $lastSlash = strrpos($baseDocument, '/');
            if ($lastSlash !== false && strpos($targetDocument, substr($baseDocument, 0, $lastSlash)) === 0) {
                $targetDocument = substr($targetDocument, $lastSlash + 1);
            }
        }

        return $targetDocument . '#' . $targetFragment;
    }

    /**
     * Do not construct any instance of Util.
     */
    private function __construct()
    {
    }
}
