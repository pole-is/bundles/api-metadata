<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Model;

use Irstea\ApiMetadata\Model\BaseTypeMetadata;
use PHPUnit\Framework\TestCase;

/**
 * Class ScalarMetadataTest.
 *
 * @covers \Irstea\ApiMetadata\Model\BaseTypeMetadata
 */
class BaseTypeMetadataTest extends TestCase
{
    public function testCreate(): void
    {
        $subject = BaseTypeMetadata::create('string');

        self::assertInstanceOf(BaseTypeMetadata::class, $subject);
    }

    public function testType(): void
    {
        $subject = BaseTypeMetadata::create('string');

        self::assertEquals('string', $subject->getType());
    }

    public function testFlyWeight(): void
    {
        $subjectA = BaseTypeMetadata::create('string');
        $subjectB = BaseTypeMetadata::create('string');

        self::assertSame($subjectA, $subjectB);
    }
}
