<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Service;

use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use ApiPlatform\Core\Metadata\Resource\ResourceNameCollection;
use Irstea\ApiMetadata\Service\ResourceMap;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\MethodProphecy;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * Class ResourceMapTest.
 *
 * @covers \Irstea\ApiMetadata\Service\ResourceMap
 * @covers \Irstea\ApiMetadata\Model\Identity\ResourceIdentity
 */
class ResourceMapTest extends TestCase
{
    /** @var array<string, ResourceMetadata> */
    private $resources;

    /** @var ResourceMap */
    private $map;

    protected function setUp()
    {
        $this->resources = [
            \stdClass::class   => new ResourceMetadata('stdClass'),
            ResourceMap::class => new ResourceMetadata('ResourceMap'),
        ];

        /** @var ObjectProphecy<ResourceNameCollectionFactoryInterface> $nameFactoryProph */
        $nameFactoryProph = $this->prophesize(ResourceNameCollectionFactoryInterface::class);

        /** @var MethodProphecy $proph */
        $proph = $nameFactoryProph->create();
        $proph->willReturn(
            new ResourceNameCollection(array_keys($this->resources))
        );

        /** @var ObjectProphecy<ResourceMetadataFactoryInterface> $metadataFactoryProph */
        $metadataFactoryProph = $this->prophesize(ResourceMetadataFactoryInterface::class);

        /** @var string $stringArg */
        $stringArg = Argument::type('string');
        $proph = $metadataFactoryProph->create($stringArg);

        $that = $this;
        $proph->will(function (array $args) use ($that) {
            return $that->resources[$args[0]] ?? null;
        });

        /** @var ResourceNameCollectionFactoryInterface $nameFactory */
        $nameFactory = $nameFactoryProph->reveal();

        /** @var ResourceMetadataFactoryInterface $metadataFactory */
        $metadataFactory = $metadataFactoryProph->reveal();

        $this->map = new ResourceMap($nameFactory, $metadataFactory);
    }

    public function testGetByShortName()
    {
        $resourceId = $this->map->getByShortName('stdClass');
        self::assertEquals(\stdClass::class, $resourceId->getClass());
    }

    public function testGetByClassName()
    {
        $resourceId = $this->map->getByClassName(\stdClass::class);
        self::assertEquals(\stdClass::class, $resourceId->getClass());
    }

    public function testIsResourceClass()
    {
        self::assertTrue($this->map->isResourceClass(ResourceMap::class));
        self::assertFalse($this->map->isResourceClass(__CLASS__));
    }

    public function testIsResourceName()
    {
        self::assertTrue($this->map->isResourceName('ResourceMap'));
        self::assertFalse($this->map->isResourceName('ResourceMapTest'));
    }
}
