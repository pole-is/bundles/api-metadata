<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Helper;

use Irstea\ApiMetadata\Helper\PropertyInfoType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyInfo\Type;

/**
 * Class PropertyInfoTypeTest.
 */
class PropertyInfoTypeTest extends TestCase
{
    /**
     * @covers \Irstea\ApiMetadata\Helper\PropertyInfoType::create
     */
    public function testCreateFromClassName()
    {
        $type = PropertyInfoType::create(__CLASS__);

        self::assertInstanceOf(Type::class, $type);
        self::assertEquals('object', $type->getBuiltinType());
        self::assertEquals(__CLASS__, $type->getClassName());
        self::assertFalse($type->isCollection());
    }

    /**
     * @param string $typeName
     * @dataProvider providesTypeNames
     *
     * @covers \Irstea\ApiMetadata\Helper\PropertyInfoType::create
     */
    public function testCreateFromTypeName(string $typeName)
    {
        $type = PropertyInfoType::create($typeName);

        self::assertInstanceOf(Type::class, $type);
        self::assertEquals($typeName, $type->getBuiltinType());
        self::assertEquals($typeName === 'array', $type->isCollection());
    }

    /**
     * @return array
     */
    public function providesTypeNames(): array
    {
        return array_map(
            static function ($type) { return [$type]; },
            Type::$builtinTypes
        );
    }
}
