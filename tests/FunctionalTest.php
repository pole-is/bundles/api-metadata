<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests;

use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Swaggest\JsonSchema\Context;
use Swaggest\JsonSchema\InvalidValue;
use Swaggest\JsonSchema\Schema;
use Swaggest\JsonSchema\SchemaContract;
use Swaggest\JsonSchema\Structure\ObjectItem;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ReflectionTest.
 *
 * @medium
 *
 * @covers \Irstea\ApiMetadata\Bridge\RamseyUuid\UuidTypeFactory
 * @covers \Irstea\ApiMetadata\Bridge\Symfony\Bundle\DependencyInjection\IrsteaApiMetadataExtension
 * @covers \Irstea\ApiMetadata\Factory\Context
 * @covers \Irstea\ApiMetadata\Factory\Operation\NullOperationFactory
 * @covers \Irstea\ApiMetadata\Factory\Property\NullPropertyFactory
 * @covers \Irstea\ApiMetadata\Factory\Property\ResourcePropertyFactory
 * @covers \Irstea\ApiMetadata\Factory\Type\AbstractClassTypeFactory
 * @covers \Irstea\ApiMetadata\Factory\Type\BaseTypeFactory
 * @covers \Irstea\ApiMetadata\Factory\Type\BuiltinClassTypeFactory
 * @covers \Irstea\ApiMetadata\Factory\Type\CollectionTypeFactory
 * @covers \Irstea\ApiMetadata\Factory\Type\ObjectTypeFactory
 * @covers \Irstea\ApiMetadata\Factory\Type\ResourceTypeFactory
 * @covers \Irstea\ApiMetadata\Factory\Type\TypeFactoryDecorator
 * @covers \Irstea\ApiMetadata\Model\BaseTypeMetadata
 * @covers \Irstea\ApiMetadata\Model\MapMetadata
 * @covers \Irstea\ApiMetadata\Model\ObjectMetadata
 * @covers \Irstea\ApiMetadata\Model\PropertyMetadata
 * @covers \Irstea\ApiMetadata\Model\ResourceMetadata
 * @covers \Irstea\ApiMetadata\Model\FormattedStringMetadata
 */
class FunctionalTest extends WebTestCase
{
    use RefreshDatabaseTrait;

    /** @var Client */
    private $client;

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->client = self::createClient();
    }

    /**
     * @param string $method
     * @param string $url
     *
     * @return mixed
     */
    private function fetchRawJson(string $method, string $url)
    {
        $this->client->request($method, $url, [], [], ['HTTP_ACCEPT' => 'application/json']);

        /** @var Response $response */
        $response = $this->client->getResponse();
        self::assertTrue(
            $response->isSuccessful(),
            sprintf(
                "unexpected error on %s %s: %d\n\nHeaders:\n%s\n\nBody:\n%s\n",
                $method,
                $url,
                $response->getStatusCode(),
                $response->headers,
                $response->getContent()
            )
        );
        self::assertJson($response->getContent());

        return $response->getContent();
    }

    public function testGetResourceMetadataCollection()
    {
        $collectionJson = $this->fetchRawJson('GET', '/metadata');
        $collection = json_decode($collectionJson, true);

        self::assertIsArray($collection);

        self::assertEquals(
            [
                '$id'       => 'http://localhost/metadata',
                'resources' => [
                    'User'   => ['$ref' => 'http://localhost/metadata/User#'],
                    'Person' => ['$ref' => 'http://localhost/metadata/Person#'],
                ],
            ],
            $collection
        );
    }

    /**
     * @throws \Exception
     *
     * @return SchemaContract
     */
    public function testJsonSchemaValidity(): SchemaContract
    {
        $options = new Context();
        $options->setRemoteRefProvider(new TestRemoteRefProvider($this->client));
        $options->schemasCache = new \SplObjectStorage();

        $schema = Schema::import('/metadata/Person', $options);
        self::assertInstanceOf(SchemaContract::class, $schema);

        return $schema;
    }

    /**
     * @depends      testJsonSchemaValidity
     * @dataProvider fetchPeople
     *
     * @param $person
     * @param SchemaContract $schema
     */
    public function testValidateData($person, SchemaContract $schema)
    {
        try {
            /** @var ObjectItem $result */
            $result = $schema->in($person);
            self::assertNotNull($result);
        } catch (InvalidValue $err) {
            self::fail($err->getMessage());
        }
    }

    /**
     * @return array
     */
    public function fetchPeople(): array
    {
        $this->client = self::createClient();
        $json = $this->fetchRawJson('GET', '/people');

        return array_map(function ($data) {
            return [$data];
        }, json_decode($json, false));
    }
}
