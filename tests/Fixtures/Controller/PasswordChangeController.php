<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Fixtures\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Irstea\ApiMetadata\Tests\Fixtures\Entity\LocalUser;
use Irstea\ApiMetadata\Tests\Fixtures\Entity\PasswordChangeRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PasswordChangeController.
 */
class PasswordChangeController
{
    /** @var SerializerInterface */
    private $serializer;

    /** @var ValidatorInterface */
    private $validator;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * PasswordChangeController constructor.
     *
     * @param SerializerInterface    $serializer
     * @param ValidatorInterface     $validator
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager
    ) {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request   $req
     * @param LocalUser $data
     *
     * @return Response
     */
    public function __invoke(Request $req, LocalUser $data): Response
    {
        /** @var PasswordChangeRequest $dto */
        $dto = $this->serializer->deserialize($req->getContent(), PasswordChangeRequest::class, 'json');

        $violations = $this->validator->validate($dto);
        if (count($violations) > 0) {
            return JsonResponse::fromJsonString(
                $this->serializer->serialize($violations, 'json'),
                Response::HTTP_BAD_REQUEST
            );
        }

        $data->changePassword($dto);

        $this->entityManager->persist($data);
        $this->entityManager->flush();

        return Response::create('', Response::HTTP_NO_CONTENT);
    }
}
