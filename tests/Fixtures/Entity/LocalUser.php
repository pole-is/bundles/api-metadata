<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Fixtures\Entity;

use ApiPlatform\Core\Annotation as API;
use Assert\Assertion;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class LocalUser.
 *
 * @ORM\Entity
 * @API\ApiResource(
 *     itemOperations={
 *         "get",
 *         "put",
 *         "password"={
 *             "method": "POST",
 *             "path": "/users/{id}/password",
 *             "controller"=PasswordChangeController::class,
 *             "input"=PasswordChangeRequest::class,
 *             "output"=false
 *         },
 *     },
 *     collectionOperations={
 *          "get"
 *     }
 * )
 */
class LocalUser extends User
{
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @API\ApiProperty(readable=false, writable=false)
     */
    private $password;

    /**
     * @param string $password
     *
     * @return bool
     */
    public function checkPassword(string $password): bool
    {
        return $this->password !== null ? \password_verify($password, $this->password) : $password === '';
    }

    /**
     * @param PasswordChangeRequest $req
     */
    public function changePassword(PasswordChangeRequest $req): void
    {
        if (!$this->checkPassword($req->getOld())) {
            throw new \InvalidArgumentException('Old passwords mismatch');
        }
        $hash = \password_hash($req->getNew(), \PASSWORD_DEFAULT);
        Assertion::string($hash, 'could not hash password');
        $this->password = $hash;
    }
}
