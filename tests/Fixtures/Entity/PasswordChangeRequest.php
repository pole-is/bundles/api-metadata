<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Fixtures\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PasswordUpdate.
 */
final class PasswordChangeRequest
{
    /**
     * @var string
     * @Assert\NotNull
     */
    private $old;

    /**
     * @var string
     * @Assert\NotBlank
     */
    private $new;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\EqualTo(propertyPath="new", message="Passwords mismatch")
     */
    private $confirm;

    /**
     * PasswordChangeRequest constructor.
     *
     * @param string $old
     * @param string $new
     * @param string $confirm
     */
    public function __construct(string $old, string $new, string $confirm)
    {
        $this->old = $old;
        $this->new = $new;
        $this->confirm = $confirm;
    }

    /**
     * Get old.
     *
     * @return string
     */
    public function getOld(): string
    {
        return $this->old;
    }

    /**
     * Get new.
     *
     * @return string
     */
    public function getNew(): string
    {
        return $this->new;
    }
}
