<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Fixtures\Entity;

use ApiPlatform\Core\Annotation as API;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Person.
 *
 * @ORM\Entity
 * @API\ApiResource(
 *     itemOperations={"get"},
 *     collectionOperations={"get"}
 * )
 */
class Person
{
    /**
     * @var UuidInterface
     * @ORM\Column(type="uuid")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $lastName;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Email
     */
    private $email;

    /**
     * @var Address
     * @ORM\Embedded(class=Address::class)
     * @Assert\Valid
     */
    private $address;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetimetz", nullable=true)
     * @Assert\DateTime
     */
    private $birthDate;

    /**
     * @var User|null
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="person", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * Person constructor.
     *
     * @param string $firstName
     * @param string $lastName
     *
     * @throws \Exception
     */
    public function __construct(string $firstName, string $lastName)
    {
        $this->id = Uuid::uuid4();
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->address = Address::create();
    }

    /**
     * Get id.
     *
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * Get phoneNumber.
     *
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * Set phoneNumber.
     *
     * @param string|null $phoneNumber
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * Get address.
     *
     * @return Address|null
     */
    public function getAddress(): Address
    {
        return $this->address;
    }

    /**
     * Set address.
     *
     * @param Address|null $address
     */
    public function setAddress(?Address $address)
    {
        $this->address = $address ?: Address::create();
    }

    /**
     * Get birthDate.
     *
     * @return \DateTime|null
     */
    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    /**
     * Set birthDate.
     *
     * @param \DateTime|null $birthDate
     */
    public function setBirthDate(?\DateTime $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set user.
     *
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        if ($user === $this->user) {
            return;
        }

        $this->user = $user;
        if ($user) {
            $user->setPerson($this);
        }
    }

    /**
     * @return \Generator<string>
     */
    public function getInitials(): \Generator
    {
        $words = preg_split('/\W+/', $this->firstName . ' ' . $this->lastName);
        if (!\is_iterable($words)) {
            return;
        }
        foreach ($words as $word) {
            if ($word !== '') {
                yield \strtoupper($word[0]);
            }
        }
    }

    /**
     * @param \DateTimeInterface|null $when
     *
     * @return int|null
     */
    public function getAge(\DateTimeInterface $when = null): ?int
    {
        if (!$this->birthDate) {
            return null;
        }
        if (!$when) {
            $when = new \DateTime();
        }

        return $when->diff($this->birthDate)->y;
    }
}
