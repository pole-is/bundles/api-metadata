<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Address.
 *
 * @ORM\Embeddable
 */
class Address
{
    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $street;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $postalCode;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Country
     */
    private $country;

    /**
     * Address constructor.
     *
     * @param string|null $street
     * @param string|null $postalCode
     * @param string|null $city
     * @param string|null $country
     */
    private function __construct(?string $street, ?string $postalCode, ?string $city, ?string $country)
    {
        $this->street = $street;
        $this->postalCode = $postalCode;
        $this->city = $city;
        $this->country = $country;
    }

    /**
     * Get street.
     *
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * Get postalCode.
     *
     * @return string|null
     */
    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    /**
     * Get city.
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * Get country.
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $street
     * @param string|null $postalCode
     * @param string|null $city
     * @param string|null $country
     *
     * @return Address
     */
    public static function create(string $street = null, ?string $postalCode = null, ?string $city = null, ?string $country = null): Address
    {
        static $null = null;

        if (!$street && !$postalCode && !$city && !$country) {
            if (!$null) {
                $null = new self(null, null, null, null);
            }

            return $null;
        }

        return new self($street, $postalCode, $city, $country);
    }
}
