<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

use Irstea\ApiMetadata\Tests\Fixtures\TestKernel;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\HttpFoundation\Request;

set_time_limit(0);

require dirname(__DIR__, 2) . '/bootstrap.php';

umask(0000);

/* @noinspection ForgottenDebugOutputInspection */
Debug::enable();

/* @noinspection ForgottenDebugOutputInspection */
ErrorHandler::register();

$kernel = new TestKernel('dev', true);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
