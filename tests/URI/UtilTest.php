<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\URI;

use Irstea\ApiMetadata\URI\Util;
use PHPUnit\Framework\TestCase;

/**
 * Class UtilTest.
 *
 * @covers \Irstea\ApiMetadata\URI\Util::makeRelativeURI
 */
class UtilTest extends TestCase
{
    /**
     * @param string|null $expected
     * @param string      $documentURI
     * @param string      $targetURI
     *
     * @dataProvider getFragmentInDocumentTestCases
     */
    public function testGetFragmentInDocument(?string $expected, string $documentURI, string $targetURI)
    {
        self::assertEquals($expected, Util::getFragmentInDocument($documentURI, $targetURI));
    }

    /**
     * @return array
     */
    public function getFragmentInDocumentTestCases(): array
    {
        return [
            ['#/', 'blabla', '#/'],
            ['#', 'blabla', '#'],
            ['#/some/path', 'blabla', '#/some/path'],
            ['#/some/path', 'blabla', 'blabla#/some/path'],
            [null, 'other', 'blabla#/some/path'],
        ];
    }
}
