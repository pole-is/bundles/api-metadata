<?php declare(strict_types=1);
/*
 * This file is part of "irstea/api-metadata".
 *
 * Copyright (C) 2019 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\ApiMetadata\Tests\JSON;

use Irstea\ApiMetadata\JSON\Document;
use PHPUnit\Framework\TestCase;

/**
 * Class DocumentTest.
 *
 * @covers \Irstea\ApiMetadata\JSON\AbsolutePointer
 * @covers \Irstea\ApiMetadata\JSON\Document
 */
class DocumentTest extends TestCase
{
    public function testArrayAccess()
    {
        $doc = new Document('document#uri');

        $doc['document#/'] = 'patate';
        $doc['document#/a/0/k'] = 5;
        $doc['document#/a/1/k'] = 8;
        $doc['document#/b'] = 'truc';
        $doc['document#/a/5/k'] = 7;
        $doc['document#'] = ['foo' => 'bar'];

        unset($doc['document#/a/5/k']);
        unset($doc['external#/a/5/k']);

        self::assertTrue(isset($doc['document#/a/0/k']));
        self::assertEquals(5, $doc['document#/a/0/k']);

        self::assertFalse(isset($doc['document#/a/5/k']));

        self::assertFalse(isset($doc['external#/a/0/k']));
        self::assertNull($doc['external#/a/0/k']);
    }

    public function testToArray()
    {
        $doc = new Document('document#uri');

        $doc['document#/a'] = [2 => 7];
        $doc['document#/a/0/k'] = 5;
        $doc['document#/a/1/k'] = 8;
        $doc['document#/b'] = 'truc';
        $doc['document#'] = ['foo' => 'bar'];

        self::assertEquals(
            [
                '$id' => 'document',
                'foo' => 'bar',
                'a'   => [
                    ['k' => 5],
                    ['k' => 8],
                    7,
                ],
                'b' => 'truc',
            ],
            $doc->toArray()
        );
    }
}
